import express from "express";

let app=express();

app.get("/Poema", (peticion, respuesta)=>{

    respuesta.send("Yo no hablo de venganzas\n ni perdones, el olvido\n es la única venganza\n y el único perdón.\n -Jorge Luis Borges");
});

app.get("/Versiculo", (peticion2, respuesta2)=>{

    respuesta2.send("El señor mismo marchará al frente de ti y estará contigo; nunca te dejará, ni te abandorá. No temas ni te desanimes. -Deuteronomio 31:8");
});

app.listen(3000, ()=>{console.log("¡El servidor está activo!")} );